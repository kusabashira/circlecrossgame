package main

import "github.com/nsf/termbox-go"

type Scene interface {
	Action(e termbox.Event) (nextScene Scene, err error)
	View() (err error)
}
