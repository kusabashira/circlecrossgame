package main

import "github.com/nsf/termbox-go"

type Result struct {
	winner     Cell
	board      *Board
	isReplay   bool
}

func NewResult(board *Board, winner Cell) *Result {
	return &Result{
		winner:    winner,
		board:     board,
		isReplay:  true,
	}
}

func (r *Result) Action(e termbox.Event) (nextScene Scene, err error) {
	nextScene = r
	switch e.Type {
	case termbox.EventKey:
		switch e.Key {
		case termbox.KeyCtrlC, termbox.KeyEsc:
			nextScene = nil
		case termbox.KeyArrowLeft, termbox.KeyArrowRight:
			r.isReplay = !(r.isReplay)
		case termbox.KeySpace:
			if r.isReplay {
				firstPutter.Reverse()
				nextScene = NewGame()
			} else {
				nextScene = nil
			}
		}
	case termbox.EventError:
		nextScene = nil
		err = e.Err
	}
	return
}

func (r *Result) View() error {
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	termbox.HideCursor()
	tbxSetBaseView()

	cenX, cenY := tbxGetCenterXY()
	switch r.winner {
	case Circle:
		tbxSetText(cenX+4, cenY-1, "Circle win!",
			termbox.ColorGreen, termbox.ColorDefault)
	case Cross:
		tbxSetText(cenX+4, cenY-1, "Cross win!",
			termbox.ColorGreen, termbox.ColorDefault)
	default:
		tbxSetText(cenX+4, cenY-1, "Draw...",
			termbox.ColorGreen, termbox.ColorDefault)
	}

	if r.isReplay {
		tbxSetText(cenX+4, cenY+1, "Replay",
			termbox.ColorBlack, termbox.ColorGreen)
		tbxSetText(cenX+12, cenY+1, "Quit",
			termbox.ColorGreen, termbox.ColorDefault)
	} else {
		tbxSetText(cenX+4, cenY+1, "Replay",
			termbox.ColorGreen, termbox.ColorDefault)
		tbxSetText(cenX+12, cenY+1, "Quit",
			termbox.ColorBlack, termbox.ColorGreen)
	}

	for x := 0; x < 3; x++ {
		for y := 0; y < 3; y++ {
			termbox.SetCell(cenX+x-1, cenY+y-1, r.board[x][y].View(),
				termbox.ColorGreen, termbox.ColorDefault)
		}
	}

	termbox.Flush()
	return nil
}
