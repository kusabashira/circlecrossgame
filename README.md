CicleCrossGame
====================
This is a CLI game application.
a.k.a marubatu game.

![](screenshot.png)

License
====================
MIT License

Usage
====================
hit the following in your terminal.
(Please press the ESC key to exit)

	$ circlecrossgame

It will start this.

Operation
====================

###Menu
| Key    |                       |
|--------|-----------------------|
| Right  | Toggle first putter   |
| Left   | Toggle first putter   |
| Space  | Start this game       |
| ESC    | Quit this application |
| Ctrl+C | Quit this application |

###Game
| Key    |                            |
|--------|----------------------------|
| Up     | Move to the top            |
| Down   | Move to the bottom         |
| Right  | Move to the right          |
| Left   | Move to the left           |
| Space  | Put Cell on current cursor |
| ESC    | Quit this application      |
| Ctrl+C | Quit this application      |

###Result
| Key    |                       |
|--------|-----------------------|
| Right  | Toggle isReplay       |
| Left   | Toggle isReplay       |
| Space  | Decide isReplay       |
| ESC    | Quit this application |
| Ctrl+C | Quit this application |

Author
====================
Kusabashira <kusabashira227@gmail.com>
