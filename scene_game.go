package main

import "github.com/nsf/termbox-go"

type Game struct {
	curX        int
	curY        int
	board       *Board
	putter      Cell
}

func NewGame() *Game {
	return &Game{
		board:  NewBoard(),
		putter: firstPutter,
	}
}

func (g *Game) SupressCursorXY() {
	switch {
	case g.curX < 0:
		g.curX = 0
	case 2 < g.curX:
		g.curX = 2
	}
	switch {
	case g.curY < 0:
		g.curY = 0
	case 2 < g.curY:
		g.curY = 2
	}
}

func (g *Game) Winner() Cell {
	b := g.board

	for x := 0; x < 3; x++ {
		if b[x][0] != Empty && b[x][0] == b[x][1] && b[x][1] == b[x][2] {
			return b[x][0]
		}
	}

	for y := 0; y < 3; y++ {
		if b[0][y] != Empty && b[0][y] == b[1][y] && b[1][y] == b[2][y] {
			return b[0][y]
		}
	}

	if (b[0][0] != Empty && b[0][0] == b[1][1] && b[1][1] == b[2][2]) ||
		(b[0][0] != Empty && b[2][0] == b[1][1] && b[1][1] == b[0][2]) {
		return b[1][1]
	}

	return Empty
}

func (g *Game) IsFinished() bool {
	noEmpty := 0
	for x := 0; x < 3; x++ {
		for y := 0; y < 3; y++ {
			if g.board[x][y] == Empty {
				noEmpty++
			}
		}
	}
	if noEmpty == 0 {
		return true
	}
	winner := g.Winner()
	if winner != Empty {
		return true
	}
	return false
}

func (g *Game) Put() {
	g.SupressCursorXY()
	if g.board[g.curX][g.curY] != Empty {
		return
	}
	g.board[g.curX][g.curY] = g.putter
	g.putter.Reverse()

}

func (g *Game) Action(e termbox.Event) (nextScene Scene, err error) {
	nextScene = g
	switch e.Type {
	case termbox.EventKey:
		switch e.Key {
		case termbox.KeyCtrlC, termbox.KeyEsc:
			nextScene = nil
		case termbox.KeyArrowLeft:
			g.curX--
		case termbox.KeyArrowRight:
			g.curX++
		case termbox.KeyArrowUp:
			g.curY--
		case termbox.KeyArrowDown:
			g.curY++
		case termbox.KeySpace:
			g.Put()
		}
	case termbox.EventError:
		nextScene = nil
		err = e.Err
	}
	if g.IsFinished() {
		winner := g.Winner()
		switch winner {
		case Circle:
			noCircleWin++
		case Cross:
			noCrossWin++
		}
		nextScene = NewResult(g.board, g.Winner())
	}
	g.SupressCursorXY()
	return
}

func (g *Game) View() error {
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	tbxSetBaseView()

	cenX, cenY := tbxGetCenterXY()
	if g.putter == Circle {
		tbxSetText(cenX+4, cenY-1, "Circle",
			termbox.ColorBlack, termbox.ColorGreen)
		tbxSetText(cenX+4, cenY+1, "Cross",
			termbox.ColorGreen, termbox.ColorDefault)
	} else {
		tbxSetText(cenX+4, cenY-1, "Circle",
			termbox.ColorGreen, termbox.ColorDefault)
		tbxSetText(cenX+4, cenY+1, "Cross",
			termbox.ColorBlack, termbox.ColorGreen)
	}

	realCurX, realCurY := cenX+g.curX-1, cenY+g.curY-1
	termbox.SetCursor(realCurX, realCurY)
	for x := 0; x < 3; x++ {
		for y := 0; y < 3; y++ {
			termbox.SetCell(cenX+x-1, cenY+y-1, g.board[x][y].View(),
				termbox.ColorGreen, termbox.ColorDefault)
		}
	}

	termbox.Flush()
	return nil
}
