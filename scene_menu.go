package main

import "github.com/nsf/termbox-go"

type Menu struct {
	curSelect Cell
}

func NewMenu() *Menu {
	return &Menu{
		curSelect: Circle,
	}
}

func (m *Menu) Action(e termbox.Event) (nextScene Scene, err error) {
	nextScene = m
	switch e.Type {
	case termbox.EventKey:
		switch e.Key {
		case termbox.KeyCtrlC, termbox.KeyEsc:
			nextScene = nil
		case termbox.KeyArrowLeft, termbox.KeyArrowRight:
			m.curSelect.Reverse()
		case termbox.KeySpace:
			firstPutter = m.curSelect
			nextScene = NewGame()
		}
	case termbox.EventError:
		nextScene = nil
		err = e.Err
	}
	return
}

func (m *Menu) View() error {
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	termbox.HideCursor()
	tbxSetBaseView()

	cenX, cenY := tbxGetCenterXY()

	tbxSetText(cenX+4, cenY-1, "Which is first?",
		termbox.ColorGreen, termbox.ColorDefault)

	if m.curSelect == Circle {
		tbxSetText(cenX+4, cenY+1, "Circle",
			termbox.ColorBlack, termbox.ColorGreen)
		tbxSetText(cenX+12, cenY+1, "Cross",
			termbox.ColorGreen, termbox.ColorDefault)
	} else {
		tbxSetText(cenX+4, cenY+1, "Circle",
			termbox.ColorGreen, termbox.ColorDefault)
		tbxSetText(cenX+12, cenY+1, "Cross",
			termbox.ColorBlack, termbox.ColorGreen)
	}

	termbox.Flush()
	return nil
}
