package main

type Cell int

const (
	Empty Cell = iota
	Circle
	Cross
)

func (c *Cell) Reverse() {
	switch *c {
	case Circle:
		*c = Cross
	case Cross:
		*c = Circle
	default:
		*c = Empty
	}
}

func (c *Cell) View() rune {
	switch *c {
	case Circle:
		return 'o'
	case Cross:
		return 'x'
	default:
		return ' '
	}
}

type Board [3][3]Cell

func NewBoard() *Board {
	return &Board{}
}
