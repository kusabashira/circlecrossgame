package main

import (
	"fmt"

	"github.com/nsf/termbox-go"
)

func tbxGetCenterXY() (x, y int) {
	winW, winH := termbox.Size()
	return winW / 2, winH / 2
}

func tbxSetBorder(topX, topY, bottomX, bottomY int, color termbox.Attribute) {
	for x := topX; x <= bottomX; x++ {
		for y := topY; y <= bottomY; y++ {
			if (x == topX || x == bottomX) ||
				(y == topY || y == bottomY) {
				termbox.SetCell(x, y, ' ', color, color)
			}
		}
	}
}

func tbxSetText(topX, topY int, s string, fg, bg termbox.Attribute) {
	for i, r := range s {
		termbox.SetCell(topX+i, topY, r, fg, bg)
	}
}

func tbxSetBaseView() {
	cenX, cenY := tbxGetCenterXY()

	tbxSetText(cenX-8, cenY-4, "Circle Cross Game",
		termbox.ColorGreen, termbox.ColorDefault)

	tbxSetText(cenX-13, cenY+4, "(Press Ctrl+c or ESC to exit)",
		termbox.ColorGreen, termbox.ColorDefault)

	if noCircleWin != 0 || noCrossWin != 0 {
		tbxSetText(cenX-12, cenY-1, fmt.Sprintf("Circle: %d", noCircleWin),
			termbox.ColorGreen, termbox.ColorDefault)
		tbxSetText(cenX-12, cenY+1, fmt.Sprintf(" Cross: %d", noCrossWin),
			termbox.ColorGreen, termbox.ColorDefault)
	}

	tbxSetBorder(cenX-2, cenY-2, cenX+2, cenY+2, termbox.ColorGreen)
}
