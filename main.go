package main

import (
	"os"

	"github.com/nsf/termbox-go"
)

var firstScene Scene = NewMenu()
var firstPutter = Empty

var noCircleWin = 0
var noCrossWin = 0

func mainloop() error {
	var e termbox.Event
	var scene Scene = firstScene
	for {
		scene.View()
		e = termbox.PollEvent()
		nextScene, err := scene.Action(e)
		switch {
		case err != nil:
			return err
		case nextScene == nil:
			return nil
		}
		scene = nextScene
	}

	return nil
}

func termMain() (exitCode int, err error) {
	if err := termbox.Init(); err != nil {
		return 1, err
	}
	defer termbox.Close()

	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	if err := mainloop(); err != nil {
		return 1, err
	}

	return 0, nil
}

func main() {
	exitCode, err := termMain()
	if err != nil {
		os.Stderr.WriteString(err.Error())
	}
	os.Exit(exitCode)
}
